package demo.test;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;
import webdriver.BaseTest;
import demo.test.forms.*;
import webdriver.PropertiesResourceManager;
import webdriver.elements.Button;
import webdriver.elements.Label;

public class DemoTest extends BaseTest {

	
	public void runTest() {
/*
		logger.step(1);
		browser.navigate("http://www.tut.by/");
		logger.step(2);
		TutSearchForm tsf = new TutSearchForm();
		tsf.assertLogo();
		logger.step(3);
		tsf.searchFor("A1QA");
		logger.step(4);
		browser.waitForPageToLoad();
		logger.step(5);
		tsf.searchAssert();
*/
		PropertiesResourceManager bundle = new PropertiesResourceManager("test.properties");

		logger.step(1);
		// open main page
		browser.navigate(bundle.getProperty("main_page"));
		logger.step(2);

		// create navbar page
		Navbar onf = new OnlinerNavbarForm();
		onf.goTo(bundle.getProperty("navbar_products_item"));
		browser.waitForPageToLoad();

		// list of products themes
		Navbar opnf = new OnlinerProductsNavbarForm();
		logger.step(3);
		opnf.goTo(bundle.getProperty("product_theme"));
		logger.step(4);
		browser.waitForPageToLoad();

		// search the project
		ProductSearchForm psf = new ProductSearchForm();
		logger.step(5);

		// set properties
		psf.setPrice(bundle.getProperty("tv_max_price"));
		logger.step(6);
		psf.setYear(bundle.getProperty("tv_min_year"));
		logger.step(7);
		psf.setCompany(bundle.getProperty("tv_company"));
		logger.step(8);
		psf.setMinInch(bundle.getProperty("tv_min_inch"));
		logger.step(9);
		psf.setMaxInch(bundle.getProperty("tv_max_inch"));
		logger.step(10);
		psf.click();
		logger.step(11);

		browser.waitForPageToLoad();
		logger.step(12);

		// set price usd dollars
		new Button(By.xpath("//li[@data-currency='USD']"), "y.e. page opened").click();
		logger.step(13);
		browser.waitForPageToLoad();
		logger.step(14);


		// parse result
		ProductSearchResultForm psrf = new ProductSearchResultForm();
		logger.step(15);
		psrf.assertResult(bundle.getProperty("tv_company"),
				bundle.getProperty("tv_max_price"),
				bundle.getProperty("tv_min_inch"),
				bundle.getProperty("tv_max_inch"));
		logger.step(16);


		browser.exit();
	}



}
