package demo.test.forms;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;
import webdriver.BaseForm;
import webdriver.elements.Button;
import webdriver.elements.Label;
import webdriver.elements.TextBox;

/**
 * Created by gur on 28.11.14.
 */
public class ProductSearchForm extends BaseForm implements ProductSearchInterface {
    private static String formlocator="//form[@name='filter' and @method='post']";

    private Button btnSubmitSearch = new Button (By.xpath("//input[@name='search']"),"search product button");

    private void setValueToTextBox(By by, String value) {
        new TextBox(by, by.toString() + " set to " + value).setText(value);
    }

    public void setPrice(String value) {
        setValueToTextBox(By.id(priceTextBoxId), value);
    }

    public void setYear(String value) {
        setValueToTextBox(By.id(yearTextBoxId), value);
    }

    private void setSelect(String id, String value) {
        new Select(browser.getDriver().findElement(By.id(id))).selectByVisibleText(value);
    }

    public void setCompany(String value) {
        setSelect(companyId, value);
    }

    public void setMaxInch(String value) {
        setSelect(maxInchId, value);
    }

    public void setMinInch(String value) {
        setSelect(minInchId, value);
    }

    public ProductSearchForm() {
        super(By.xpath(formlocator), "Product seach form");
    }

    public void click() {
        btnSubmitSearch.click();
    }
}
