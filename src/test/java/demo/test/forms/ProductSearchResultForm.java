package demo.test.forms;

import org.apache.tools.ant.util.regexp.Regexp;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import webdriver.BaseForm;
import webdriver.elements.Button;
import webdriver.elements.TextBox;

import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by gur on 28.11.14.
 */
public class ProductSearchResultForm extends BaseForm implements ProductSearchInterface {
    private static String formlocator="//form[@name='product_list' and @method='post']";

    public void assertResult(String companyName, String price, String minInch, String maxInch) {

        WebElement mainElement = browser.getDriver().findElement(By.xpath(formlocator));

        List<WebElement> els = mainElement.findElements(By.className(companyNameClass));
        for(WebElement e : els) {
 //           if(els.get(0) == e)
   //             continue;
            String companyNameT = e.getText();//.findElement(By.tagName("a")).getText();
            assert(companyNameT.toLowerCase().contains(companyName.toLowerCase()));
        }

        els = mainElement.findElements(By.className(priceClass));
        for(WebElement e : els) {
            assert(Double.parseDouble(e.getText().replace(" ", "").split("-")[0]) <= Double.parseDouble(price));
        }

        els = mainElement.findElements(By.className(inchesClass));
        for(WebElement e : els) {
            Pattern patternInch = Pattern.compile("(\\d+\\.*\\d*)\"");
            Matcher matcherInch = patternInch.matcher(e.getText());
            if(matcherInch.find()) {
                //System.out.println(e.getText());
                double tmp = Double.parseDouble(matcherInch.group(0).replace("\"", ""));
                Matcher minMatcher = patternInch.matcher(minInch);
                minMatcher.find();
                double min = Double.parseDouble(minMatcher.group(0).replace("\"", ""));
                Matcher maxMatcher = patternInch.matcher(maxInch);
                maxMatcher.find();
                double max = Double.parseDouble(maxMatcher.group(0).replace("\"", ""));
                assert(tmp <= max && min <= tmp);
            } else assert(false);
        }




    }

    public ProductSearchResultForm() {
        super(By.xpath(formlocator), "Products list form");
    }
}
