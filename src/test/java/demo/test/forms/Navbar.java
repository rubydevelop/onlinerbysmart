package demo.test.forms;

import org.openqa.selenium.By;
import webdriver.BaseForm;

/**
 * Created by gur on 28.11.14.
 */
public abstract class Navbar extends BaseForm {
    public abstract void goTo(String href);

    public Navbar(By by, String name) {
        super(by, name);
    }

}
