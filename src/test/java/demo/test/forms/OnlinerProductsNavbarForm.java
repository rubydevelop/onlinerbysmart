package demo.test.forms;

import org.openqa.selenium.By;
import webdriver.BaseForm;
import webdriver.elements.Button;

public class OnlinerProductsNavbarForm extends Navbar {
    private static String formlocator="//div[@class='b-whbd']";

    public void goTo(String href) {
        new Button(By.xpath("//a[@href='http://catalog.onliner.by/" + href + "' and @class='catalog-bar__link catalog-bar__link_strong']"),"search button").click();
    }

    public OnlinerProductsNavbarForm() {
        super(By.xpath(formlocator), "Onliner navbar of products themes");
    }

}
