package demo.test.forms;

import org.openqa.selenium.By;

import webdriver.BaseForm;
import webdriver.elements.Button;
import webdriver.elements.Label;
import webdriver.elements.TextBox;

public class OnlinerNavbarForm extends Navbar {
    private static String formlocator="//ul[@class='b-main-navigation']";

    public void goTo(String href) {
        new Button (By.xpath("//a[@href='http://" + href + ".onliner.by/' and @class='b-main-navigation__link']"),"search button").click();
    }

    public OnlinerNavbarForm() {
        super(By.xpath(formlocator), "Onliner navbar opened");
    }

}
